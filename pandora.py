import urllib, logging, urllib2, json, time
from blowfish import Blowfish
from config import client_key, proxies

def pad(s, l):
    return s + "\0" * (l - len(s))

class Pandora(object):
    api_endpoint = '://tuner.pandora.com/services/json/'

    def __init__(self):
        proxy_handler = urllib2.ProxyHandler(proxies)
        self.opener = urllib2.build_opener(proxy_handler)
        self.partnerId = self.userId = self.partnerAuthToken = None
        self.userAuthToken = self.time_offset = None
        self.blowfish_encode = Blowfish(client_key['encryptKey'])
        self.blowfish_decode = Blowfish(client_key['decryptKey'])

    def encrypt(self, s):
        return "".join([self.blowfish_encode.encrypt(pad(s[i:i+8], 8)).encode('hex') for i in xrange(0, len(s), 8)])

    def decrypt(self, s):
        return "".join([self.blowfish_decode.decrypt(pad(s[i:i+16].decode('hex'), 8)) for i in xrange(0, len(s), 16)]).rstrip('\x08')

    def request(self, method, args={}, secure=True, encrypt=True):
        protocol = 'http' if secure else 'https'

        query_params = {}

        query_params['method'] = method

        if self.time_offset:
            args['syncTime'] = int(time.time()+self.time_offset)
        if self.userAuthToken:
            args['userAuthToken'] = self.userAuthToken
        elif self.partnerAuthToken:
            args['partnerAuthToken'] = self.partnerAuthToken


        if self.partnerId:
            query_params['partner_id'] = self.partnerId
        if self.userId:
            query_params['user_id'] = self.userId
        if self.userAuthToken:
            query_params['auth_token'] = self.userAuthToken
        elif self.partnerAuthToken:
            query_params['auth_token'] = self.partnerAuthToken

        url = protocol + self.api_endpoint + '?' + urllib.urlencode(query_params)

        data = json.dumps(args)
        if encrypt:
            data = self.encrypt(data)

        req = urllib2.Request(url, data, {'Content-type': 'text/plain'})
        response_string = self.opener.open(req, timeout=10)
        response = json.load(response_string)
        self.opener.close()


        if response['stat'] == 'fail':
            fail_code = response['code']
            raise Exception('Request failed. Code:' + str(fail_code))

        return response


    def connect(self, login, password):       
        response = self.request('auth.partnerLogin', {
            'deviceModel': client_key['deviceModel'],
            'username': client_key['username'],
            'password': client_key['password'],
            'version': client_key['version']
            }, encrypt=False)
    
        partner = response['result']


        self.partnerId = partner['partnerId']
        self.partnerAuthToken = partner['partnerAuthToken']

        pandora_time = int(self.decrypt(partner['syncTime'])[4:14])
        self.time_offset = pandora_time - time.time()

        result = self.request('auth.userLogin', {'username': login, 'password': password, 'loginType': 'user'})['result']
        self.userAuthToken = result['userAuthToken']
        self.userId = result['userId']

    def user_getBookmarks(self):
        return self.request('user.getBookmarks')

    def user_getStationList(self):
        return self.request('user.getStationList')['result']['stations']

    def station_getPlaylist(self, station):
        return self.request('station.getPlaylist', args={'stationToken': station['stationToken']})
