from pandora import Pandora
from config import credits, download_path

import urllib, re, os


class PandoraFetcher(object):
	def __init__(self, credits, download_path):
		self.credits = credits
		self.download_path = download_path
		self.stationList = {}
		self.pandora = Pandora()
		self.pandora.connect(self.credits['login'], self.credits['password'])
		self.list_stations()

	def list_stations(self):
		self.stationList = self.pandora.user_getStationList()
		for x in range(1, len(self.stationList)):
			print str(x) + ':' + self.stationList[x]['stationName']

	def save_file(self, link, name):
		full_path = self.download_path + self.currentStation['stationName']

		if not os.path.isdir(full_path) and not os.path.exists(full_path):
			os.mkdir(download_path + self.currentStation['stationName'])
		urllib.urlretrieve (link, full_path + '/' + name)

	def format_song_name(self, song):
		return song['songName'] + ' - ' + song['artistName'] + '.m4a'

	def unescape_string(self, string):
		return re.sub('[\/:*?"<>|]', '', string)

	def run(self):	
		while True:
			var = str(raw_input("Enter your choice:"))
			if var == 's':
				self.list_stations()
			else:
				stationId = int(var)
				if stationId < len(self.stationList):
					self.currentStation = self.stationList[int(var)]
					print 'Current station: ' + self.currentStation['stationName']
					while True:
						playlist = self.pandora.station_getPlaylist(self.currentStation)
						if 'result' in playlist:
							for song in playlist['result']['items']:
								if 'artistName' in song:	
									file_name = self.format_song_name(song)
									print ' - Downloading ' + file_name
									self.save_file(song['audioUrlMap']['highQuality']['audioUrl'], self.unescape_string(file_name + '.tmp'))
									os.rename(file_name + '.tmp', file_name)
						else:
							print playlist
							break
												
					

if __name__ == '__main__':
	PandoraFetcher(credits, download_path).run()
